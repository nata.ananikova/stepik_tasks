from urls import urls
import pytest


@pytest.mark.parametrize("data, expected", [(['https://stepic.org/media/attachments/lesson/24472/sample0.html',
                                             'https://stepic.org/media/attachments/lesson/24472/sample2.html'], 'Yes')])
def test_1_using_pytest(data, expected):
    assert urls(data) == expected

@pytest.mark.parametrize("data, expected", [(['https://stepic.org/media/attachments/lesson/24472/sample0.html',
                                             'https://stepic.org/media/attachments/lesson/24472/sample1.html'], 'No')])
def test_2_using_pytest(data, expected):
    assert urls(data) == expected


@pytest.mark.parametrize("data, expected", [(['https://stepic.org/media/attachments/lesson/24472/sample1.html',
                                             'https://stepic.org/media/attachments/lesson/24472/sample2.html'], 'Yes')])
def test_3_using_pytest(data, expected):
    assert urls(data) == expected