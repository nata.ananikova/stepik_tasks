import json

js = json.loads(input())
lst = []
for i in js:
    for v in i.values():
        lst.append(v)
global_namespace = {lst[i]: lst[i + 1] for i in range(0, len(lst), 2)}


for v, k in global_namespace.items():
    Name1, *Names = v, k
    global_namespace[Name1.strip()] = k
    for a in k:
        if a not in global_namespace.keys():
            global_namespace[a] = []
for key, value in global_namespace.items():
    for j in value:
        if j in global_namespace.keys():
            global_namespace[key] += global_namespace[j]

k_l, v_l = [k for k in global_namespace.keys()], [v for v in global_namespace.values()]
v_set = []
for i in v_l:
    i = set(i)
    for j in i:
        v_set.append(j)
joinedlist = k_l + v_set
d = {}
for word in joinedlist:
    if word in d.keys():
        d[word] += 1
    else:
        d[word] = 1

sorted_dict = {key: value for key, value in sorted(d.items())}
for w, n in sorted_dict.items():
    print(f'{w} :', n)








