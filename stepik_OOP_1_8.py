from random import random


class Server:
    """для описания работы серверов в сети
    Соответственно в объектах класса Server должны быть локальные свойства:
    buffer - список принятых пакетов (изначально пустой);
    ip - IP-адрес текущего сервера.
    """
    buffer: list
    ip = random

    def __init__(self):
        self.buffer = []
        self.ip = Server.ip

    @staticmethod
    def send_data(data):
        """для отправки информационного пакета data (объекта класса Data)
        с указанным IP-адресом получателя (пакет отправляется роутеру и
        сохраняется в его буфере - локальном свойстве buffer);
        """
        Router.buffer.append(id(data))

    def get_data(self):
        """возвращает список принятых пакетов (если ничего принято не было,
        то возвращается пустой список) и очищает входной буфер;
        """
        ret_buffer = self.buffer
        self.buffer.clear()
        return ret_buffer

    @staticmethod
    def get_ip():
        """возвращает свой IP-адрес."""
        return


class Router:
    """для описания работы роутеров в сети (в данной задаче полагается один роутер).
        И одно обязательное локальное свойство (могут быть и другие свойства):
        buffer - список для хранения принятых от серверов пакетов (объектов класса Data).
        """
    buffer = []
    link_dict = {}

    def __init__(self):
        self.buffer = []

    def link(self, server):
        """для присоединения сервера server (объекта класса Server) к роутеру"""
        link_dict = dict.fromkeys([Server.ip], 1)

    def unlink(self, server):
        """для отсоединения сервера server (объекта класса Server) от роутера"""
        del self.link_dict[Server.ip]

    def send_data(self):
        """для отправки всех пакетов (объектов класса Data) из буфера роутера
        соответствующим серверам (после отправки буфер должен очищаться)"""
        ret_buffer = self.buffer
        self.buffer.clear()
        return ret_buffer

class Data:
    """для описания пакета информации
        Наконец, объекты класса Data должны содержать, два следующих локальных свойства:
        data - передаваемые данные (строка);
        ip - IP-адрес назначения.
        """
    def __init__(self, data, ip):
        self.data = data
        self.ip = ip


assert hasattr(Router, 'link') and hasattr(Router, 'unlink') and hasattr(Router,
                                                                         'send_data'), "в классе Router присутсвутю не все методы, указанные в задании"
assert hasattr(Server, 'send_data') and hasattr(Server, 'get_data') and hasattr(Server,
                                                                                'get_ip'), "в классе Server присутсвутю не все методы, указанные в задании"

router = Router()
sv_from = Server()
sv_from2 = Server()
router.link(sv_from)
router.link(sv_from2)
router.link(Server())
router.link(Server())
sv_to = Server()
router.link(sv_to)
sv_from.send_data(Data("Hello", sv_to.get_ip()))
sv_from2.send_data(Data("Hello", sv_to.get_ip()))
sv_to.send_data(Data("Hi", sv_from.get_ip()))
router.send_data()
msg_lst_from = sv_from.get_data()
msg_lst_to = sv_to.get_data()

assert len(router.buffer) == 0, "после отправки сообщений буфер в роутере должен очищаться"
assert len(sv_from.buffer) == 0, "после получения сообщений буфер сервера должен очищаться"

assert len(msg_lst_to) == 2, "метод get_data вернул неверное число пакетов"

assert msg_lst_from[0].data == "Hi" and msg_lst_to[
    0].data == "Hello", "данные не прошли по сети, классы не функционируют должным образом"

assert hasattr(router, 'buffer') and hasattr(sv_to,
                                             'buffer'), "в объектах классов Router и/или Server отсутствует локальный атрибут buffer"

router.unlink(sv_to)
sv_from.send_data(Data("Hello", sv_to.get_ip()))
router.send_data()
msg_lst_to = sv_to.get_data()
assert msg_lst_to == [], "метод get_data() вернул неверные данные, возможно, неправильно работает метод unlink()"

