class multifilter:

    def judge_half(pos, neg):
        if pos >= neg:
            return True

    def judge_any(pos, neg):
        if pos >= 1:
            return True

    def judge_all(pos, neg):
        if neg == 0:
            return True

    def __init__(self, iterable, *funcs, judge=judge_any):
        self.iterable = iterable
        self.funcs = funcs
        self.judge = judge

    def count_res(self, a):
        res = [f(a) for f in self.funcs]
        return res.count(True), res.count(False)

    def __iter__(self):
        for i in self.iterable:
            for _ in self.funcs:
                pos, neg = self.count_res(i)
                if self.judge(pos, neg):
                    yield i
                    break


def mul2(x):
    return x % 2 == 0


def mul3(x):
    return x % 3 == 0


def mul5(x):
    return x % 5 == 0


a = [i for i in range(31)]

print(list(multifilter(a, mul2, mul3, mul5)))
#[0, 2, 3, 4, 5, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30]

print(list(multifilter(a, mul2, mul3, mul5, judge=multifilter.judge_half)))
# #[0, 6, 10, 12, 15, 18, 20, 24, 30]

print(list(multifilter(a, mul2, mul3, mul5, judge=multifilter.judge_all)))
# #[0, 30]
