# 3
class DataBase:
    pk = 1
    title = "Классы и объекты"
    author = "Сергей Балакирев"
    views = 14356
    comments = 12


# 4
class Goods:
    title = "Мороженое"
    weight = 154
    tp = "Еда"
    price = 1024


Goods.price = 2048
Goods.inflation = 100


# 5
class Car:
    pass


setattr(Car, 'model', "Тойота")
setattr(Car, 'color', "Розовый")
setattr(Car, 'number', "П111УУ77")

print(Car.__dict__['color'])


# 6
class Notes:
    uid = 1005435
    title = "Шутка"
    author = "И.С. Бах"
    pages = 2


print(getattr(Notes, 'author'))


# 7
class Dictionary:
    rus = "Питон"
    eng = "Python"


try:
    print(getattr(Dictionary, 'rus_word'))
except:
    print('False')


# 8
class TravelBlog:
    total_blogs = 0


tb1 = TravelBlog()
tb1.name = 'Франция'
tb1.days = 6
TravelBlog.total_blogs += 1
tb2 = TravelBlog()
tb2.name = 'Италия'
tb2.days = 5
TravelBlog.total_blogs += 1


# 9
class Figure:
    type_fig = 'ellipse'
    color = 'red'


fig1 = Figure()
setattr(fig1, 'start_pt', (10, 5))
setattr(fig1, 'end_pt', (100, 20))
setattr(fig1, 'color', 'blue')
delattr(fig1, 'color')
print(*fig1.__dict__.keys())


# 10
class Person:
    name ='Сергей Балакирев'
    job = 'Программист'
    city = 'Москва'


p1 = Person()
if 'job' in p1.__dict__:
    print('True')
else:
    print('False')
