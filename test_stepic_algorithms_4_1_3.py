from stepik_algorithms_4_1_3 import fraction_knapsack
import pytest


@pytest.mark.parametrize("data, expected", [(1, 1), (2, 2), (3, [1, 2]), (4, [1, 3]), (5, [1, 4]),
                                            (6, [1, 2, 3]), (7, [1, 2, 4]), (8, [1, 2, 5]), (9, [1, 2, 6])])
def test_1_using_pytest(data, expected):
    assert fraction_knapsack(data) == expected


@pytest.mark.parametrize("data, expected", [(10, [1, 2, 3, 4]), (11, [1, 2, 3, 5]),
                                            (12, [1, 2, 3, 6]), (13, [1, 2, 3, 7]), (14, [1, 2, 3, 8])])
def test_2_using_pytest(data, expected):
    assert fraction_knapsack(data) == expected


@pytest.mark.parametrize("data, expected", [(15, [1, 2, 3, 4, 5]), (16, [1, 2, 3, 4, 6]), (17, [1, 2, 3, 4, 7]),
                                            (18, [1, 2, 3, 4, 8]), (19, [1, 2, 3, 4, 9]), (20, [1, 2, 3, 4, 10])])
def test_3_using_pytest(data, expected):
    assert fraction_knapsack(data) == expected


@pytest.mark.parametrize("data, expected", [(21, [1, 2, 3, 4, 5, 6]), (22, [1, 2, 3, 4, 5, 7]),
                                            (23, [1, 2, 3, 4, 5, 8]), (24, [1, 2, 3, 4, 5, 9]),
                                            (25, [1, 2, 3, 4, 5, 10]), (26, [1, 2, 3, 4, 5, 11]),
                                            (27, [1, 2, 3, 4, 5, 12])])
def test_4_using_pytest(data, expected):
    assert fraction_knapsack(data) == expected
