# #list - итерируемый объект
# d = [1, 3, 5, 7, 9]
# #получаем итератор
# iterator_of_d = iter(d)
# print(iterator_of_d)
# print(next(iterator_of_d))
#
# s = 'python'
# iterator_of_s = iter(s)
# print(iterator_of_s)
# print(next(iterator_of_s))
# print(next(iterator_of_s))
#
# r = range(5)
# iterator_of_r = iter(r)
# print(iterator_of_r)
# print(next(iterator_of_r))
# print(next(iterator_of_r))

#нельзя получить итератор из неитерируемого объекта, например, int

#list comprehension - генератор списков - итерируемый объект
# n = 6
# sent = 'he is on the see'
# a = [x**2 for x in range(n)]
# a2 = [x%2 for x in range(n)]
# a_sent = [len(x) for x in sent.split() if len(x)>=3]
# a2_sent = ['четное' if len(x)%2 == 0 else 'нечетное' for x in sent.split()]
# print(a2_sent)

#генераторы множеств и генераторы словарей
# a_set = {x**2 for x in range(6)}
# print(a_set)
# a_dict = {x: x**2 for x in range(6)}
# print(a_dict)

# выражения генераторы
# a = (x**2 for x in range(6))
# print(a)
# print(next(a))
# print(next(a))
# for i in a:
#     print(i)
# for i in a:
#     print(i)
# print(list(a))
# print(list(a))
# print(sum(a))
# print(sum(a))
# print(len(a))

# функция-генератор
# def get_return():
#     for x in [1, 2, 3, 4]:
#         return x


# def get_yield():
#     for x in [1, 2, 3, 4]:
#         yield x
#
#
# def average_yield():
#     for x in range(10):
#         a = range(x, 10)
#         yield sum(a)/len(a)


# print(get_return())

# f = get_yield()
# print(next(f))
# print(next(f))
# print(next(f))
# print(next(f))
#
# average = average_yield()
# print(next(average))
# print(next(average))
# print(next(average))
# print(next(average))
# print(list(average))