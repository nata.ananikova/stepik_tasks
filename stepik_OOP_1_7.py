#6
class Factory:
    @staticmethod
    def build_sequence():
        return []

    @staticmethod
    def build_number(string):
        return float(string)


class Loader:
    @staticmethod
    def parse_format(string, factory):
        seq = factory.build_sequence()
        for sub in string.split(","):
            item = factory.build_number(sub)
            seq.append(item)

        return seq


res = Loader.parse_format("1, 2, 3, -5, 10", Factory)
print(res)


#7
from string import ascii_lowercase, digits

class TextInput:

    CHARS = "абвгдеёжзийклмнопрстуфхцчшщьыъэюя " + ascii_lowercase
    CHARS_CORRECT = CHARS + CHARS.upper() + digits

    def __init__(self, name, size=10):
        self.name = name
        self.size = size
        self.check_name(self.name)

    def get_html(self):
        return f"<p class='login'>{self.name}: <input type='text' size={self.size} />"

    @classmethod
    def check_name(cls, name):
        if not 3 <= len(name) <= 50 and cls.CHARS and cls.CHARS_CORRECT:
            raise ValueError("некорректное поле name")


class PasswordInput:

    CHARS = "абвгдеёжзийклмнопрстуфхцчшщьыъэюя " + ascii_lowercase
    CHARS_CORRECT = CHARS + CHARS.upper() + digits

    def __init__(self, name, size=10):
        self.name = name
        self.size = size
        self.check_name(self.name)

    def get_html(self):
        return f"<p class='password'>{self.name}: <input type='text' size={self.size} />"

    @classmethod
    def check_name(cls, name):
        if not 3 <= len(name) <= 50 and cls.CHARS and cls.CHARS_CORRECT:
            raise ValueError("некорректное поле name")


class FormLogin:
    def __init__(self, lgn, psw):
        self.login = lgn
        self.password = psw

    def render_template(self):
        return "\n".join(['<form action="#">', self.login.get_html(), self.password.get_html(), '</form>'])


# эти строчки не менять
login = FormLogin(TextInput("Логин"), PasswordInput("Пароль"))
html = login.render_template()


#8
import re
from string import ascii_lowercase, digits

class CardCheck:

    CHARS_FOR_NAME = ascii_lowercase.upper() + digits

    @staticmethod
    def check_card_number(number):
        number = number.rstrip()
        pattern = r'\d{4}-\d{4}-\d{4}-\d{4}'
        if re.findall(pattern, number) and len(number) == 19:
            return True
        else:
            return False

    @classmethod
    def check_name(cls, name):
        for i in name:
            if i not in cls.CHARS_FOR_NAME:
                return False
            elif len(name.split()) != 2:
                return False
            return True


is_number = CardCheck.check_card_number("1234-5678-9012-0000")
is_name = CardCheck.check_name("SERGEI BALAKIREV")
assert CardCheck.check_card_number("1234-5678-9012-0000") == True
assert CardCheck.check_name("SERGEI BALAKIREV") == True
assert CardCheck.check_card_number("1244-5678-9012-0000-5643") == False
assert CardCheck.check_name("SERGEI BALAKIREV NAM") == False
assert CardCheck.check_name("СЕРГЕЙ BALAKIREV") == False


#9
class Video:

    def create(self, name):
        self.name = name

    def play(self):
        print(f"воспроизведение видео {self.name}")


class YouTube:
    videos = []

    @classmethod
    def add_video(cls, video):
        cls.videos.append(video)

    @classmethod
    def play(cls, video_indx):
        Video.play(cls.videos[video_indx])


v1 = Video()
v2 = Video()
v1.create("Python")
v2.create("Python ООП")
YouTube.add_video(v1)
YouTube.add_video(v2)
YouTube.play(0)
YouTube.play(1)

assert hasattr(YouTube, 'add_video') and hasattr(YouTube, 'play')


#10
class AppStore:
    app_list = []

    def add_application(self, app):
        self.app_list.append(app)

    def remove_application(self, app):
        self.app_list.remove(app)

    def block_application(self, app):
        app.blocked = True

    def total_apps(self):
        return len(self.app_list)


class Application:
    def __init__(self, name, blocked=False):
        self.name = name
        self.blocked = blocked


store = AppStore()
app_youtube = Application("Youtube")
store.add_application(app_youtube)
store.remove_application(app_youtube)
print(AppStore.app_list)


#11
class Viber:
    msg_lst = []

    @staticmethod
    def add_message(msg):
        Viber.msg_lst.append(msg)

    @staticmethod
    def remove_message(msg):
        Viber.msg_lst.remove(msg)

    @staticmethod
    def set_like(msg):
        if msg.fl_like == True:
            msg.fl_like = False
        else:
            msg.fl_like = True

    @staticmethod
    def show_last_message(msg_int: int):
        return Viber.msg_lst[msg_int:]

    @staticmethod
    def total_messages():
        return len(Viber.msg_lst)


class Message:

    def __init__(self, text, fl_like = False):
        self.text = text
        self.fl_like = fl_like


msg = Message("Всем привет!")
Viber.add_message(msg)
Viber.add_message(Message("Это курс по Python ООП."))
Viber.add_message(Message("Что вы о нем думаете?"))
Viber.set_like(msg)
Viber.remove_message(msg)

assert msg.fl_like == True


