# 2
class Money:
    def __init__(self, quantity):
        self.quantity = quantity
        setattr(self, 'money', quantity)

    def money(self):
        return self.quantity


my_money = Money(100)
your_money = Money(1000)

assert my_money.money == 100 and your_money.money == 1000


# 3
class Point:

    def __init__(self, x, y, color='black'):
        self.x = x
        self.y = y
        self.color = color


p1 = Point(10, 20)
p2 = Point(12, 5, 'red')

points = [Point(i, i, color='yellow') if i == 3 else Point(i, i) for i in range(1, 2000, 2)]

assert len(points) == 1000
assert points[1].color == 'yellow'

# 4
import random


class Line:
    def __init__(self, a, b, c, d):
        setattr(self, 'sp', (a, b))
        setattr(self, 'ep', (c, d))

    def sp(self, a, b):
        self.sp = (a, b)

    def ep(self, c, d):
        self.ep = (c, d)


class Rect:
    def __init__(self, a, b, c, d):
        sp = (a, b)
        ep = (c, d)


class Ellipse:
    def __init__(self, a, b, c, d):
        sp = (a, b)
        ep = (c, d)


elements = []
for i in range(217):
    a, b, c, d = random.randint(-255, 255), random.randint(-255, 255), \
                 random.randint(-255, 255), random.randint(-255, 255)
    elements.append(random.choice([Line(0, 0, 0, 0), Rect(a, b, c, d), Ellipse(a, b, c, d)]))

l = Line(1, 2, 3, 4)

assert l.sp == (1, 2) and l.ep == (3, 4)


# 5
class TriangleChecker:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def is_triangle(self):
        if not all(map(lambda x: type(x) in (int, float), (self.a, self.b, self.c))):
            return 1
        if not all(map(lambda x: x > 0, (self.a, self.b, self.c))):
            return 1
        elif self.a + self.b <= self.c or self.a + self.c <= self.b or self.c + self.b <= self.a:
            return 2
        else:
            return 3


a, b, c = map(int, input().split())
tr = TriangleChecker(a, b, c)
print(tr.is_triangle())


# 6
class Graph:
    def __init__(self, data, is_show=True):
        self.data = data.copy()
        self.is_show = is_show

    def set_data(self, data):
        self.data = data.copy()
        return " ".join(map(str, self.data))

    def show_table(self):
        if not self.is_show == False:
            return self.set_data(self.data)
        else:
            print("Отображение данных закрыто")

    def show_graph(self):
        if not self.is_show == False:
            print(f"Графическое отображение данных: {self.set_data(self.data)}")
        else:
            print("Отображение данных закрыто")

    def show_bar(self):
        if not self.is_show == False:
            print(f"Столбчатая диаграмма: {self.set_data(self.data)}")
        else:
            print("Отображение данных закрыто")

    def set_show(self, fl_show):
        self.is_show = fl_show


data_graph = list(map(int, input().split()))
gr = Graph(data_graph)
gr.show_bar()
gr.set_show(fl_show=False)
gr.show_table()


# 7
class CPU:
    def __init__(self, name, fr):
        self.name = name
        self.fr = fr


class Memory:
    def __init__(self, name, volume):
        self.name = name
        self.volume = volume


class MotherBoard:
    def __init__(self, name, cpu, *mem_slots):
        self.name = name
        self.cpu = cpu
        self.total_mem_slots = 4
        self.mem_slots = mem_slots[:self.total_mem_slots][0]

    def get_config(self):
        mem_str = "; ".join([f"{x.name} - {x.volume}" for x in mb.mem_slots])

        return [f"Материнская плата: {mb.name}",
                f"Центральный процессор: {mb.cpu.name}, {mb.cpu.fr}",
                f"Слотов памяти: {mb.total_mem_slots}",
                f"Память: {mem_str}"]


exact_cpu = CPU('asus', 1333)
mem1, mem2 = Memory('Kingstone', 4000), Memory('Kingstone', 4000)
mb = MotherBoard('Asus', exact_cpu, [mem1, mem2])


# 8
class Cart:
    goods = []

    def add(self, gd):
        self.goods.append(gd)

    def remove(self, indx):
        self.goods.pop(indx)

    def get_list(self):
        return [f'{x.name}: {x.price}' for x in self.goods]


class Table:
    def __init__(self, name, price):
        self.name = name
        self.price = price


class TV:
    def __init__(self, name, price):
        self.name = name
        self.price = price


class Notebook:
    def __init__(self, name, price):
        self.name = name
        self.price = price


class Cup:
    def __init__(self, name, price):
        self.name = name
        self.price = price


cart = Cart()
tv1 = TV("samsung", 1111)
tv2 = TV("LG", 1234)
t = Table("ikea", 2345)
n1 = Notebook("msi", 5433)
n2 = Notebook("apple", 542)
c = Cup("keepcup", 43)
cart.add(tv1)
cart.add(tv2)
cart.add(t)
cart.add(n1)
cart.add(n2)
cart.add(c)

# 9
import sys


class ListObject:
    def __init__(self, data):
        self.data = data
        self.next_obj = None

    def link(self, obj):
        self.next_obj = obj


lst_in = list(map(str.strip, sys.stdin.readlines()))
head_obj = ListObject(lst_in[-1])
for i in reversed(lst_in[:-1]):
    n = ListObject(i)
    n.link(head_obj)
    head_obj = n
