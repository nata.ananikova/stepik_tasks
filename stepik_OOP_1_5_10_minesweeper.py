# 10
import random


class Cell:
    def __init__(self, around_mines=0, mine=False):
        self.around_mines = around_mines
        self.mine = mine
        self.fl_open = False


class GamePole:
    def __init__(self, N, M):
        self.N = N
        self.M = M
        self.pole = [[Cell() for _ in range(self.N)] for __ in range(self.N)]
        self.init()

    def init(self):
        lst = []
        li, lj = len(self.pole), len(self.pole[0])
        for i, ind_i in enumerate(self.pole):
            for j, ind_j in enumerate(self.pole[0]):
                new = [self.pole[i - 1][j], self.pole[(i + 1) % li][j], self.pole[i][j - 1],
                       self.pole[i][(j + 1) % lj], self.pole[i - 1][j - 1], self.pole[(i + 1) % li][(j + 1) % lj],
                       self.pole[i - 1][(j + 1) % lj], self.pole[(i + 1) % li][j - 1]]
                lst.append(len([i for i in new if i == '*']))
        nested_lst = []
        index = 0
        while index < self.N:
            sub_lst = []
            for i in lst[index * self.N:self.N * index + self.N]:
                sub_lst.append(i)
            nested_lst.append(sub_lst)
            index += 1
        for m in random.sample(range(self.N * self.N), self.M):
            nested_lst[m % self.N][m // self.N] = '*'
        return nested_lst

    def show(self):
        for i in self.pole:
            for j in i:
                if j.mine and j.fl_open:
                    print('*', end=' ')
                elif not j.fl_open:
                    print('#', end=' ')
                elif j.fl_open and not j.mine:
                    print(j.around_mines, end=' ')
            print()


N, M = 10, 12
pole_game = GamePole(N, M)
