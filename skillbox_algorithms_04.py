from array import array


class DynArray:
    def __init__(self):
        self.data = array('i', (0 for _ in range(1)))
        self.length = 0

    def size(self):
        return self.length

    def sett(self, ind, value):
        self.data[ind] = value

    def get(self, ind):
        return self.data[ind]

    def add(self, x):
        lenn = len(self.data)
        if self.length == lenn:
            data2 = array('i', (0 for _ in range(self.length*2)))
            for i in range(0, self.length):
                data2[i] = self.data[i]
            self.data = data2
        self.data[self.length] = x
        self.length += 1

    def remove(self):
        lenn = len(self.data)
        self.length -= 1
        if self.length*4 <= lenn:
            data2 = array('i', (0 for _ in range(lenn // 2)))
            for i in range(0, self.length):
                data2[i] = self.data[i]
            self.data = data2

    def insert(self, x, ind):
        self.add(0)
        for i in range(self.length - 1, ind, -1):
            self.data[i] = self.data[i-1]
        self.data[ind] = x


# class Node:
#     def __init__(self, x):
#         self.x = x
#         self.nextNode = None
#
#
# if __name__ == '__main__':
#     start = Node(None)
#
#     for i in range(0, 11):
#         n = Node(i)
#         n.nextNode = start
#         start = n
#     summ = 0
#     t = Node(None)
#     t = start
#     while t.nextNode != None:
#         summ += t.x
#         t = t.nextNode


# def check(s):
#     st = []
#     for c in s:
#         if c == '(' or c == '{' or c == '[':
#             st.append(c)
#         else:
#             if not st:
#                 return False
#             if st[-1] == '(' and c == ')':
#                 st.pop()
#                 continue
#             if st[-1] == '[' and c == ']':
#                 st.pop()
#                 continue
#             if st[-1] == '{' and c == '}':
#                 st.pop()
#                 continue
#             return False
#         if st:
#             return False
#         return True
#
#
# if __name__ == '__main__':
#
#     print(check('({[(}]}}}'))



class state:

    def __init__(self,a: int, b: int, st: int):

        self.a = a
        self.b = b
        self.st = st


def power2 (a: int, b: int) -> int:
    st = []
    st.append(state(a, b, 0))
    ret = 0
    while st:
        a = st[-1].a
        b = st[-1].b
        pos = st[-1].st
        st.pop()
        if pos == 0:
            if b == 0:
                ret = 1
                continue
            if b % 2 == 0:
                st.append(state(a, b, 1))
                st.append(state(a, int(b/2), 0))
            else:
                st.append(state(a, b, 2))
                st.append(state(a, b-1, 0))
            continue
        elif pos == 1:
            ret = ret * ret
            continue
        else:
            ret = ret * a
            continue
    return ret


if __name__ == "__main__":
    print(power2(2, 3))
