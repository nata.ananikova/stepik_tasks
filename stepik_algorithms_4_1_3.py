# По данному числу 1≤n≤109 1 \le n \le 10^9 1≤n≤109 найдите максимальное число k k k,
# для которого n n n можно представить как сумму k k k
# различных натуральных слагаемых. Выведите в первой строке число k k k, во второй — k k k слагаемых.


def fraction_knapsack(reader):
    if reader <= 2:
        return reader
    else:
        count = 1
        lst, reader_lst = [], []
        for i in range(1, reader):
            reader -= count
            if reader > count:
                lst.append(count)
                reader_lst.append(reader)
            else:
                lst.append(reader_lst[-1])
                break
            count += 1
        return lst


if __name__ == '__main__':

    print(fraction_knapsack(int(input())))




