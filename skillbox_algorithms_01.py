# ages = [10, 6, 15, 93, 42, 7, 32]
# list.sort(ages)
# max_age = ages[-1]
# print("Max_age = %d\n" % max_age)
#
# age_mom = 45
# age_dad = 47
# max_age = max(age_mom, age_dad)
# print("Max_age = %d\n" % max_age)
#
# age_mom = 39
# age_dad = 41
# max_age = max(age_mom, age_dad)
# age_grandma = 83
# max_age = max(max_age, age_grandma)
# print("Max_age = %d\n" % max_age)
#
# ages = [10, 6, 15, 93, 42, 7, 32]
# max_age = 0
# for age in ages:
#     max_age = max(max_age, age)
#
# max_age2 = 0
# for age in ages:
#     if age < max_age:
#         print(max_age, max(max_age2, age))


# def find_max_under_boundary(input_array, top_boundary):
#     current_max = float('-inf')
#     for k in input_array:
#         if k < top_boundary:
#             current_max = max(current_max, k)
#     return current_max
#
#
# def find_top_elements(input_array, number_of_elements):
#     top_elements = []
#     previous_max = float('inf')
#     for i in range(number_of_elements):
#         current_max = find_max_under_boundary(input_array, previous_max)
#         previous_max = current_max
#         top_elements.append(current_max)
#     return top_elements
#
#
# array = [34, 94, 33, 102, 45, 10, 14]
# top3 = find_top_elements(array, 3)
# print("Top 3: ", top3)


# def find_unique(phone_numbers):
#     uniq_numbers = []
#     for current_number in phone_numbers:
#         already_exist = False
#         for existing_numbers in uniq_numbers:
#             if existing_numbers == current_number:
#                 already_exist = True
#                 break
#         if not already_exist:
#             uniq_numbers.append(current_number)
#     return uniq_numbers
#
#
# if __name__ == '__main__':
#     phone_numbers = [+79161002030, +79255558877, +79219990000, +79161002030]
#     uniq_numbers = find_unique(phone_numbers)
#     print("Unique numbers: ", uniq_numbers)


# def find_unique_sorted(phone_numbers):
#     prev_number = phone_numbers[0]
#     unique_numbers = []
#     for i in range(1, len(phone_numbers)):
#         if phone_numbers[i] != prev_number:
#             unique_numbers.append(prev_number)
#             prev_number = phone_numbers[i]
#     unique_numbers.append(prev_number)
#     return unique_numbers
#
#
# if __name__ == '__main__':
#     phone_numbers = [
#     +79000000000,+79000000000,
#     +79000000001,
#     +79000000002,+79000000002,
#     +79000000003,+79000000003,+79000000003,+79000000003,
#     +79000000004]
#     unique_numbers = find_unique_sorted(phone_numbers)
#     print("Unique numbers:", unique_numbers)


# def find_unique_sorted(phone_numbers):
#     unique_numbers = []
#     next_number = phone_numbers[0]
#     for i in range(len(phone_numbers)-1):
#         if phone_numbers[i+1] != next_number:
#             unique_numbers.append(next_number)
#             next_number = phone_numbers[i+1]
#     unique_numbers.append(next_number)
#     return unique_numbers
#
#
# if __name__ == '__main__':
#     phone_numbers = [
#     +79000000000,+79000000000,
#     +79000000001,
#     +79000000002,+79000000002,
#     +79000000003,+79000000003,+79000000003,+79000000003,
#     +79000000004]
#     unique_numbers = find_unique_sorted(phone_numbers)
#     print("Unique numbers:", unique_numbers)


# def find_unique_sorted(phone_numbers):
#     unique_numbers = []
#     for i in range(len(phone_numbers)):
#         already_exist = False
#         for existing_numbers in unique_numbers:
#             if existing_numbers == phone_numbers[i]:
#                 already_exist = True
#                 break
#         if not already_exist:
#             unique_numbers.append(phone_numbers[i])
#     return unique_numbers
#
#
# if __name__ == '__main__':
#     phone_numbers = [
#     +79000000000,+79000000000,
#     +79000000001,
#     +79000000002,+79000000002,
#     +79000000003,+79000000003,+79000000003,+79000000003,
#     +79000000004]
#     unique_numbers = find_unique_sorted(phone_numbers)
#     print("Unique numbers:", unique_numbers)