#3
# class Clock:
#
#     def __init__(self, __clock=0):
#         self.__clock = __clock
#
#     def set_time(self, tm):
#         if Clock.check_time(tm):
#             self.__clock = tm
#
#     def get_time(self):
#         return self.__clock
#
#     @staticmethod
#     def check_time(tm):
#         return True if type(tm) == int and 0 <= tm < 100000 else False
#
#
# clock = Clock(4530)
# assert isinstance(clock, Clock) and hasattr(Clock, 'set_time') and hasattr(Clock, 'get_time')
# assert clock.get_time() == 4530


#4
# class Money:
#
#     money: int
#
#     def __init__(self, __money):
#         """приватная локальная переменная money (целочисленная) для хранения количества
#         денег (своя для каждого объекта класса Money)"""
#         self.__money = __money
#
#     def set_money(self, money):
#         """публичный метод set_money(money) для передачи нового значения
#         приватной локальной переменной money (изменение выполняется только
#         если метод check_money(money) возвращает значение True)"""
#         if Money.__check_money(money):
#             self.__money = money
#
#     def get_money(self):
#         """публичный метод get_money() для получения текущего объема средств (денег)"""
#         return self.__money
#
#     def add_money(self, mn):
#         '''публичный метод add_money(mn) для прибавления средств из
#         объекта mn класса Money к средствам текущего объекта'''
#         self.__money += mn.get_money()
#
#     def __check_money(money):
#         """приватный метод класса check_money(money) для проверки
#         корректности объема средств в параметре money (возвращает True,
#         если значение корректно и False - в противном случае)
#         Проверка корректности выполняется по критерию:
#         параметр money должен быть целым числом, больше или равным нулю."""
#         return True if type(money) == int and 0 <= money else False
#
#
# mn_1 = Money(10)
# mn_2 = Money(20)
# mn_1.set_money(100)
# mn_2.add_money(mn_1)
# m1 = mn_1.get_money()    # 100
# m2 = mn_2.get_money()    # 120
# assert mn_1.get_money() == 100


#6
# class Book:
#
#     def __init__(self, author, title, price):
#         self.__title = title
#         self.__author = author
#         self.__price = price
#
#     def set_title(self, title):
#         self.__title = title
#
#     def set_author(self, author):
#         self.__author = author
#
#     def set_price(self, price):
#         self.__price = price
#
#     def get_title(self):
#         return self.__title
#
#     def get_author(self):
#         return self.__author
#
#     def get_price(self):
#         return self.__price
