def hh_test(N, M, X):
    total_home_cost = 0
    x_up_every_period = 1000
    for n in N:
        if n < M*len(N)/M:
            total_home_cost += X+x_up_every_period
    return total_home_cost


print(hh_test(range(0, 30), 10, 10000))
