# Задача на программирование: кодирование Хаффмана
#
#
# По данной непустой строке s s s длины не более 104 10^4 104, состоящей
# из строчных букв латинского алфавита, постройте оптимальный беспрефиксный код.
# В первой строке выведите количество различных букв k k k, встречающихся в строке, и размер
# получившейся закодированной строки. В следующих k k k строках запишите коды букв в формате
# "letter: code". В последней строке выведите закодированную строку.


class node:
    def __init__(self, freq, symbol, left=None, right=None):
        self.freq = freq
        self.symbol = symbol
        self.left = left
        self.right = right
        self.huff = ''


def first(lett):
    d = {}
    for i in lett:
        if i not in d.items():
            d[i] = lett.count(i)
    sort_d = {k: v for k, v in sorted(d.items(), key=lambda item: item[1])}
    sort = {}
    for k, v in sort_d.items():
        sort.setdefault(v, []).append(k)
    return sort


d = {}


def printNodes(node, val=''):
    newVal = val + str(node.huff)
    if node.left:
        printNodes(node.left, newVal)
    if node.right:
        printNodes(node.right, newVal)
    if not node.left and not node.right:
        d[node.symbol] = newVal


if __name__ == '__main__':
    input = input()
    f = first(input)
    freq, chars, nodes = [], [], []
    for k, v in f.items():
        for j in v:
            freq.append(k)
            chars.append(j)

    for x in range(len(chars)):
        nodes.append(node(freq[x], chars[x]))

    while len(nodes) > 1:
        nodes = sorted(nodes, key=lambda x: x.freq)
        left = nodes[0]
        right = nodes[1]
        left.huff = 0
        right.huff = 1

        newNode = node(left.freq + right.freq, left.symbol + right.symbol, left, right)
        nodes.remove(left)
        nodes.remove(right)
        nodes.append(newNode)

    printNodes(nodes[0])
    for key, value in d.items():
        if value == '':
            d.update({key: 0})
    solution = []
    for let in input:
        solution.append(str(d.get(let)))

    print(len(chars), sum(len(i) for i in solution))
    for key, value in d.items():
        print(f'{key}: {value}')
    print(*solution, sep='')
