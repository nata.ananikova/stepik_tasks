from typing import List, Optional
#
#
# class NamesHash:
#     def __init__(self):
#         self.values = [None] * 8
#
#     def get_index_by_key(self, key: str) -> int:
#         return len(key)
#
#     def get_value_by_key(self, key: str) -> int:
#         index = self.get_index_by_key(key)
#         return self.values[index]
#
#
# names_hash = NamesHash()
# names_hash.values[2] = 14  # Ия
# names_hash.values[3] = 99  # Аня
# names_hash.values[4] = 30  # Миша
# names_hash.values[5] = 42  # Антон
# names_hash.values[6] = 87  # Владик
# names_hash.values[7] = 71  # Николай
#
#
# class PhoneCash:
#     def __init__(self):
#         self.values = [None] * 40
#
#     def get_index_by_key(self, key: int) -> int:
#         return key % 40
#
#     def get_value_by_key(self, key: int) -> int:
#         index = self.get_index_by_key(key)
#         return self.values[index]
#
#
# class DynamicArray:
#     def __init__(self):
#         self.values: List[Optional[int]] = [None] * 8
#         self.size = 8
#         self.current_index = 0
#
#     def add(self, value: int):
#         self.values[self.current_index] = value
#         self.current_index += 1
#         if self.current_index == self.size:
#             self.resize(self.size*2)
#
#     def resize(self, new_size):
#         new_values = [None] * new_size
#         i = 0
#         while i < self.size:
#             new_values[i] = self.values[i]
#             i += 1
#         self.values = new_values
#         self.size = new_size
#
#
# class KeyValuePair:
#     key: str
#     value: str
#
#
# class HashMap:
#     def __init__(self):
#         self.values: List[Optional[int]] = [None] * 8
#         self.size = 8
#         self.number_of_elements = 0
#
#     def hash_function(self, key):
#         return 0
#
#     def add(self, key, value):
#         hashh = self.hash_function(key)
#         index = hashh % self.size
#         self.entries[index] = KeyValuePair(key=key, value=value)
#         self.number_of_elements += 1
#         if self.number_of_elements == self.size:
#             self.resize(self.size * 2)
#
#     def resize(self, new_size):
#         new_entries = [None] * new_size
#         for i in range(self.size):
#             entry = self.entries[i]
#             hashh = self.hash_function(self.key)
#             index = hashh % self.size
#             new_entries[index] = entry
#         self.entries = new_entries
#         self.size = new_size
#
#     def get(self, key):
#         hashh = self.hash_function(key)
#         index = hashh % self.size
#         entry = self.entries[index]
#         if entry is None:
#             return None
#         return entry.value


class KeyValuePair:
    key: str
    value: str

class HashMap:
    def __init__(self):
        self.entries: List[Optional[str]] = [None] * 8
        self.size = 8
        self.number_of_elements = 0

    def hash_function(self, key):
        return 0

    def add(self, key, value):
        index = self.find_good_index(key)
        self.entries[index] = KeyValuePair(key=key, value=value)
        self.number_of_elements += 1
        if self.number_of_elements == self.size:
            self.resize(self.size*2)

    def resize(self, new_size):
        new_entries = [None] * new_size
        for i in range(self.size):
            entry = self.entries[i]
            index = self.find_good_index(entry.key)
            new_entries[index] = entry
        self.entries = new_entries
        self.size = new_size

    def get(self, key):
        index = self.find_good_index(key)
        if index == -1:
            return None
        entry = self.entries[index]
        if entry is None:
            return None
        return entry.value

    def find_good_index(self, key):
        hashh = self.hash_function(key)
        index = hashh % self.size
        for i in range(self.size):
            probing_index = (index+1) % self.size
            entry = self.entries[probing_index]
            if entry is None or entry.key == key:
                return probing_index
        return -1


dictionary = {9161002030: True, 9163004050: True, 9165006070: True, 9167008090: True}
c = dictionary.get(9161002030)
print(c)


mem = ['ПреведМедвед.bmp','Кандибобер.avi','СвидетельИзФрязино.gif',
'ПреведМедвед.bmp','Кандибобер.avi','ПреведМедвед.bmp']
dictionary = {}
dictionary[mem[0]] = 1
for i in range(1, len(mem)):
    if dictionary.get(mem[i]) == None:
        dictionary[mem[i]] = 1

print(dictionary)


dictionary = {'Руки Вверх': ['Соколиный Прах Жара Июль.mp3', 'Безналога Точка Ру.mp3'],
'Жанна Агузарова' : ['Павлику Трэк Не Имеет Значения.mp3'],
'Любэ': ['Комбат, 18 мне уже.mp3', 'Потому что есть заначка у тебя.mp3']}
print(dictionary.keys())
for value in dictionary.values():
    sum = 0
    for v in value:
        sum += 1
print(sum)


