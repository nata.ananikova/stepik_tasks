from stepik_algorithms_4_3_1 import shift_up, shift_down, sort
import pytest


@pytest.mark.parametrize("data, expected", [([200, 10, 5, 500], [500, 200, 10, 5]),
    ])
def test_3_using_pytest(data, expected):
    assert sort(data) == expected
