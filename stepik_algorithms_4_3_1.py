# Задача на программирование: очередь с приоритетами
#
#
# Первая строка входа содержит число операций 1≤n≤105.
# Каждая из последующих n n n строк задают операцию одного из следующих двух типов:
#
#     Insert x , где 0≤x≤109  — целое число;
#     ExtractMax.
#
# Первая операция добавляет число x в очередь с приоритетами, вторая — извлекает максимальное число и
# выводит его.


import itertools
import sys
from heapq import heappush, heappop

pq = []                         # list of entries arranged in a heap
entry_finder = {}               # mapping of tasks to entries
REMOVED = '<removed-task>'      # placeholder for a removed task
counter = itertools.count()     # unique sequence count


def add_task(task, priority=0):
    'Add a new task or update the priority of an existing task'
    if task in entry_finder:
        remove_task(task)
    count = next(counter)
    entry = [priority, count, task]
    entry_finder[task] = entry
    heappush(pq, entry)


def remove_task(task):
    'Mark an existing task as REMOVED.  Raise KeyError if not found.'
    entry = entry_finder.pop(task)
    entry[-1] = REMOVED


def pop_task():
    'Remove and return the lowest priority task. Raise KeyError if empty.'
    while pq:
        priority, count, task = heappop(pq)
        if task is not REMOVED:
            del entry_finder[task]
            return task
    raise KeyError('pop from an empty priority queue')


def heapsort(iterable):
    h = []
    for value in iterable:
        heappush(h, value)
    return [heappop(h) for i in range(len(h))]


if __name__ == "__main__":
    extract_lst = []
    for i in range(int(sys.stdin.readline())):
        extract_lst.append(sys.stdin.readline())
    for i in extract_lst:
        if 'Insert' not in i:
            print(pop_task())
        else:
            _, integer = i.split(' ')
            add_task(int(integer))
            heapsort(int(integer))



