# 6
class AbstractClass:

    def __new__(cls, *args, **kwargs):
        return 'Ошибка: нельзя создавать объекты абстрактного класса'


obj = AbstractClass()


# 7
class SingletonFive:
    __instance = None
    cnt = 0

    def __new__(cls, *args, **kwargs):
        if cls.cnt < 5:
            cls.__instance = super().__new__(cls)
            cls.cnt += 1
            return cls.__instance
        else:
            return cls.__instance

    def __init__(self, name):
        self.name = name


objs = [SingletonFive(str(n)) for n in range(10)]
print(objs)

# 8
TYPE_OS = 2  # 1 - Windows; 2 - Linux


class DialogWindows:
    name_class = "DialogWindows"


class DialogLinux:
    name_class = "DialogLinux"


class Dialog:

    def __new__(cls, *args, **kwargs):
        if TYPE_OS == 1:
            dlg_1 = super().__new__(DialogWindows)
            setattr(dlg_1, 'name', *args)
            return dlg_1
        else:
            dlg_2 = super().__new__(DialogLinux)
            setattr(dlg_2, 'name', *args)
            return dlg_2

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return str(self)


dlg = Dialog(1)
print(dlg)


# 9
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def clone(self):
        return Point(self.x, self.y)


pt = Point('x', 'y')
pt_clone = pt.clone()
assert isinstance(pt, Point) and isinstance(pt_clone, Point)


# 10
class Factory:
    def build_sequence(self):
        return []

    def build_number(self, string):
        return float(string)


class Loader:
    def parse_format(self, string, factory):
        seq = factory.build_sequence()
        for sub in string.split(","):
            item = factory.build_number(sub)
            seq.append(item)

        return seq


ld = Loader()
res = ld.parse_format("4, 5, -6.5", Factory())
print(res)
