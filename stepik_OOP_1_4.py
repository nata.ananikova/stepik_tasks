# 4
class MediaPlayer:

    def open(self, file):
        self.filename = file

    def play(self):
        print(f"Воспроизведение {self.filename}")


media1 = MediaPlayer()
media2 = MediaPlayer()
media1.open('filemedia1')
media2.open('filemedia2')
media1.play()
media2.play()


# 5
class Graph:

    LIMIT_Y = [0, 10]

    def set_data(self, data):
        self.data = data

    def draw(self):
        for i in self.data:
            if self.LIMIT_Y[1] >= i >= self.LIMIT_Y[0]:
                print(i, end=' ')


graph_1 = Graph()
graph_1.set_data([10, -5, 100, 20, 0, 80, 45, 2, 5, 7])
graph_1.draw()


# 7
class StreamData:
    def create(self, fields, lst_values):
        if len(fields) == len(lst_values):
            self.__dict__ = dict(zip(fields,  lst_values))
            for k, v in self.__dict__.items():
                setattr(self, k, v)
                return True
        else:
            return False


class StreamReader:
    FIELDS = ('id', 'title', 'pages')

    def readlines(self):
        lst_in = list(map(str.strip, sys.stdin.readlines()))  # считывание списка строк из входного потока
        sd = StreamData()
        res = sd.create(self.FIELDS, lst_in)
        return sd, res


sr = StreamReader()
data, result = sr.readlines()


# 9
import sys

# программу не менять, только добавить два метода
lst_in = list(map(str.strip, sys.stdin.readlines()))  # считывание списка строк из входного потока


class DataBase:
    lst_data = []
    FIELDS = ('id', 'name', 'old', 'salary')

    # здесь добавлять методы
    def select(self, a, b):
        if b > len(self.lst_data):
            return self.lst_data[a:]
        else:
            return self.lst_data[a:b+1]

    def insert(self, data):
        for i in data:
            li = list(i.split(" "))
            self.__dict__ = dict(zip(self.FIELDS, li))
            self.lst_data.append(self.__dict__)


db = DataBase()
db.insert(lst_in)


# 10
class Translator:
    d = {}

    def add(self, eng, rus):
        rus_lst = self.d.setdefault(eng, [])
        if rus not in rus_lst:
            rus_lst.append(rus)
        return self.d

    def remove(self, eng):
        del self.d[eng]

    def translate(self, eng):
        return self.d[eng]


tr = Translator()
tr.add("tree", "дерево")
tr.add("digits", "1")
tr.add("digits", "2")
tr.add('digits', '1')
tr.add("car", "машина")
tr.add("car", "автомобиль")
tr.add("leaf", "лист")
tr.add("river", "река")
tr.add("go", "идти")
tr.add("go", "ехать")
tr.add("go", "ходить")
tr.add("milk", "молоко")
tr.remove('car')
tr.translate('go')
tr.translate('tree')


assert tr.translate('tree')[0] == "дерево"
assert tr.translate('digits') == ['1', '2']
assert isinstance(tr, Translator)
assert hasattr(Translator, 'add') and hasattr(Translator, 'remove') and hasattr(Translator, 'translate')

