# def binary_search(array, search):
#     left = array[0]
#     right = len(array) - 1
#     while left <= right:
#         middle = (left+right)//2
#         if search < array[middle]:
#             right = middle - 1
#         elif search > array[middle]:
#             left = middle + 1
#         else:
#             return middle
#     return -1
#
#
# if __name__ == '__main__':
#     print(binary_search([1, 2, 3, 4], 3))


# def remove_duplicates(array):
#     length = len(array)
#     i = 0
#     while i < length:
#         found = False
#         for k in range(i+1, length):
#             if array[i] == array[k]:
#                 found = True
#                 break
#         if not found:
#             i += 1
#             continue
#
#         for k in range(i+1, length):
#             array[k-1] = array[k]
#         length -= 1
#     return length
#
#
# if __name__ == '__main__':
#     array = [15, 23, 20, 5, 15, 20, 15, 20]
#     print("Original:", array)
#     print(remove_duplicates(array))


# class Player():
#     def __init__(self, rating, name):
#         self.rating = rating
#         self.name = name
#
#     # def __le__(self, other):
#     #     return other.rating > self.rating
#     #
#     # def __ge__(self, other):
#     #     return other.rating < self.rating
#     #
#     # def __eq__(self, other):
#     #     return other.rating == self.rating
#
#
# class Doka3():
#     def __init__(self):
#         self.ratings = [
#             Player(1110, 'Peter'),
#             Player(1210, 'Ivan'),
#             Player(1410, 'Dmitri'),
#             Player(2110, 'Max'),
#             Player(3110, 'Feder'),
#             Player(4110, 'Egor'),
#             Player(5110, 'Pavel'),
#             Player(5150, 'Vladimir'),
#             Player(5210, 'Jake')]
#
#     def find_spot(self, new_player):
#         left = 0
#         right = len(self.ratings) - 1
#         while left <= right:
#             middle_rating = (left + right)//2
#             if new_player.rating < self.ratings[middle_rating].rating:
#                 right = middle_rating - 1
#             elif new_player.rating > self.ratings[middle_rating].rating:
#                 left = middle_rating + 1
#         return left
#
#
# if __name__ == "__main__":
#     rating = Doka3()
#     spot = rating.find_spot(Player(1600, 'Shmike'))
#     print(spot)
