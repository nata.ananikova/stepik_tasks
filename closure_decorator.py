from functools import wraps
# #замыкание
# def average():
#     summa = 0
#     count = 0
#
#     def inner(*number):
#         nonlocal summa
#         nonlocal count
#         for i in number:
#             summa += i
#             count += 1
#         print(summa/count)
#
#     return inner
#
#
# # Press the green button in the gutter to run the script.
# if __name__ == '__main__':
#     d2 = average()
#     d2(65, 64, 65, 66)


# #замыкание
# def say_name(name):
#     @wraps(say_name)
#     def say_hello():
#         print('Hello ' + name)
#     return say_hello
#
#
# f = say_name('Python')
# f()


# #замыкание
# def counter(start=0):
#     @wraps(counter)
#     def step():
#         nonlocal start
#         start += 1
#         return start
#     return step
#
#
# print(counter(10), counter())
# c_10, c = counter(), counter(10)
# for i in range(5):
#     print(c_10(), c())

# #замыкание
# def strip_string(strip_chars=' '):
#     def do_strip(string):
#         return string.strip(strip_chars)
#     return do_strip
#
#
# str1, str2 = strip_string(), strip_string('!+')
# print(str1(' Hello python!+'), str2(' Hello python!+'))


# #декоратор
# def decorator(func):
#     def wrapper(*args, **kwargs):
#         print('before func call')
#         res = func(*args, **kwargs)
#         print('after func call')
#         return res
#     return wrapper
#
#
# def some_func(title, tag):
#     print(f'{title}')
#     return tag
#
#
# some_func = decorator(some_func)
# print(some_func('title', 'tag'))

#декоратор с параметром
def dec_with_parameter(x=2):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            print('before func call')
            res = func(*args, **kwargs)
            print('after func call')
            return res
        return wrapper
    return decorator


def some_func(tag):
    'multiple function'
    tag = tag*2
    return tag


f = dec_with_parameter(x=3)
some_func = f(some_func)
some_func_mult_2 = some_func(3)
print(some_func_mult_2)
print(some_func.__name__)
print(some_func.__doc__)


